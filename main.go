package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"sync"
	"time"
)

type Timer struct{ time time.Time }

func (t *Timer) StartTime() {
	t.time = time.Now()
}
func (t *Timer) StopTime() {
	duration := time.Since(t.time)
	fmt.Print("Milliseconds: ")
	fmt.Println(duration.Milliseconds())
	fmt.Println()
	fmt.Println()
}

func main() {
	fmt.Println("Testing Method of regex organization before computation")
	fmt.Println()
	regList := make([]string, 0)
	for i := 0; i <= 5000; i++ {
		regList = append(regList, "(The quick brown (bear|dog|fox) jumps over the lazy dog repeatedly and ((this is an email backtrack$)|(?P<T"+strconv.Itoa(i)+">this is an email subject that is way too long "+strconv.Itoa(i)+"$)))")
	}

	stringToMatch := "The quick brown fox jumps over the lazy dog repeatedly and this is an email subject that is way too long 5000"

	orTogether(regList, stringToMatch)
	individuallyRan(regList, stringToMatch)
	individuallyRanGo(regList, stringToMatch)
	individuallyRanGoWithStopChan(regList, stringToMatch)

	fmt.Println("Test matching a full email and extracting necessary data")
	fmt.Println()

	// 1451 characters
	f, err := os.ReadFile("./test_email.txt")
	if err != nil {
		panic(err)
	}
	strRegex, err := os.ReadFile("./test_email_regex.txt")
	if err != nil {
		panic(err)
	}

	extractDataFromEmail(string(f), string(strRegex))
	fullEmailStressTest(string(f), string(strRegex))

}

func fullEmailStressTest(f string, strRegex string) {
	timer := Timer{}
	// ------ TEST 2: Full Email Stress Test ---------
	fmt.Println("TEST 2: Full Email Stress Test")
	fmt.Println()

	for j := 1; j < 8; j++ {

		toRunReg := ""
		toRunStr := ""
		// slows at 7 CPU bound. Exponential increase
		for i := 0; i < j; i++ {
			toRunReg += toRunReg + strRegex
			toRunStr += toRunStr + f
		}

		fmt.Println("character count: " + strconv.Itoa(len(toRunStr)))
		fmt.Println()

		r := regexp.MustCompile(toRunReg)

		// match regex
		timer.StartTime()

		match := r.FindStringSubmatch(toRunStr)

		if len(match) > 0 {
			for i, name := range r.SubexpNames() {
				if match[i] != "" && name != "" {
					fmt.Print(".")
				}
			}
		} else {
			fmt.Println("NOT A MATCH")
		}
		fmt.Println()

		timer.StopTime()
	}
}

func extractDataFromEmail(f string, strRegex string) {
	timer := Timer{}
	// -------- TEST 1: Extract data from email -----------
	fmt.Println("TEST 1: Extract data from email")
	fmt.Println()
	r := regexp.MustCompile(strRegex)

	// match regex
	timer.StartTime()

	match := r.FindStringSubmatch(f)

	if len(match) > 0 {
		for i, name := range r.SubexpNames() {
			if match[i] != "" && name != "" {
				fmt.Println(name + ": " + match[i])
			}
		}
	} else {
		fmt.Println("NOT A MATCH")
	}
	fmt.Println()

	timer.StopTime()
}

func orTogether(regList []string, stringToMatch string) {
	timer := Timer{}
	// ------- TEST 1: Lots of Regexes |'d together --------
	fmt.Println("Test 1: regexes |'d together")
	myReg := ""
	// build regex string
	for i, r := range regList {
		if i == len(regList)-1 {
			myReg += r
		} else {
			myReg += r + "|"
		}
	}
	r, _ := regexp.Compile(myReg)

	// match regex
	timer.StartTime()

	match := r.FindStringSubmatch(stringToMatch)

	// find which template matched
	matchTemplate := ""
	for i, name := range r.SubexpNames() {
		if match[i] != "" && name != "" {
			matchTemplate = name
			break
		}
	}
	fmt.Println(matchTemplate)

	timer.StopTime()
}

func individuallyRan(regList []string, stringToMatch string) {
	timer := Timer{}
	// ------- TEST 2: Lots of Regexes individually ran --------
	fmt.Println("TEST 2: Lots of Regexes individually ran")
	compiledList := make([]*regexp.Regexp, 0)

	for _, r := range regList {
		compiledList = append(compiledList, regexp.MustCompile(r))
	}

	// match regex
	timer.StartTime()

	found := false
	for _, r := range compiledList {
		match := r.FindStringSubmatch(stringToMatch)

		// find if template matched
		if len(match) > 0 {
			for i, name := range r.SubexpNames() {
				if match[i] != "" && name != "" {
					fmt.Println(name)
					found = true
					break
				}
			}
			if found {
				break
			}
		}
	}

	timer.StopTime()
}

func individuallyRanGo(regList []string, stringToMatch string) {
	timer := Timer{}
	// ------- TEST 3: Lots of Regexes individually ran in goroutines --------
	fmt.Println("TEST 3: Lots of Regexes individually ran in goroutines")
	compiledList := make([]*regexp.Regexp, 0)

	for _, r := range regList {
		compiledList = append(compiledList, regexp.MustCompile(r))
	}

	// match regex
	timer.StartTime()

	var wg sync.WaitGroup
	for _, r := range compiledList {
		wg.Add(1)
		go func(r *regexp.Regexp) {
			match := r.FindStringSubmatch(stringToMatch)

			// find if template matched
			if len(match) > 0 {
				for i, name := range r.SubexpNames() {
					if match[i] != "" && name != "" {
						fmt.Println(name)
						break
					}
				}
			}
			wg.Done()
		}(r)
	}
	wg.Wait()

	timer.StopTime()
}

func individuallyRanGoWithStopChan(regList []string, stringToMatch string) {
	timer := Timer{}
	// ------- TEST 3: Lots of Regexes individually ran in goroutines --------
	fmt.Println("TEST 4: Lots of Regexes individually ran in goroutines with stop channel")
	compiledList := make([]*regexp.Regexp, 0)

	for _, r := range regList {
		compiledList = append(compiledList, regexp.MustCompile(r))
	}

	// match regex
	timer.StartTime()

	var wg sync.WaitGroup
	ch := make(chan bool, 1)
	done := false
	for _, r := range compiledList {
		if done {
			break
		}
		select {
		case <-ch:
			fmt.Println("FOUND")
			done = true
		default:
			wg.Add(1)
			go func(r *regexp.Regexp) {
				match := r.FindStringSubmatch(stringToMatch)

				// find if template matched
				if len(match) > 0 {
					for i, name := range r.SubexpNames() {
						if match[i] != "" && name != "" {
							fmt.Println(name)
							ch <- true
							break
						}
					}
				}
				wg.Done()
			}(r)
		}
	}
	wg.Wait()

	timer.StopTime()
}
